package de.luisdasnilpferd.varox.utils.storage;

public enum Data {

    PREFIX("§8[§cTeamChat§8] §7");

    private String stringValue;

    Data(final String stringValue){
        this.stringValue = stringValue;
    }

    @Override
    public String toString() {
        return this.stringValue;
    }
}
