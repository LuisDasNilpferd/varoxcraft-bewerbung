package de.luisdasnilpferd.varox;

import de.luisdasnilpferd.varox.commands.TeamChatCommand;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

/*********************************************************************************
 *   Urheberrechtshinweis                                                         *
 *   Copyright © Luis Garcia 2018  (Kunstname: LuisDasNilpferd)                   *
 *   Erstellt: 12.01.2019 / 19:26                                                 *
 *                                                                                *
 *   Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.             *
 *   Das Urheberrecht liegt, soweit nicht ausdrücklich anderes gekennzeichnet,    *
 *   bei Luis Garcia. Alle Rechte vorbehalten.                                    *
 *                                                                                *
 *   Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,          *
 *   öffentlichen Zugänglichmachung oder andere Nutzung                           *
 *   bedarf der ausdrücklichen, schriftlichen Zustimmung von Luis Garcia.         *
 *********************************************************************************/

public class BewerbungsSystem extends Plugin {

    private static BewerbungsSystem bewerbungsSystem;

    private PluginManager pluginManager;

    @Override
    public void onLoad() {
        bewerbungsSystem = this;
    }

    @Override
    public void onEnable() {
        this.init();
    }

    @Override
    public void onDisable() {

    }

    private void init(){
        this.pluginManager = this.getProxy().getPluginManager();

        this.registerListener();
        this.registerCommands();
    }

    private void registerListener() {

    }

    private void registerCommands() {
        new TeamChatCommand(bewerbungsSystem);
    }

    public static BewerbungsSystem getBewerbungsSystem() {
        return bewerbungsSystem;
    }

    public PluginManager getPluginManager() {
        return pluginManager;
    }
}
