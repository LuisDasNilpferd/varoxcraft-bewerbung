package de.luisdasnilpferd.varox.commands;

import de.luisdasnilpferd.varox.BewerbungsSystem;
import de.luisdasnilpferd.varox.utils.storage.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/*********************************************************************************
 *   Urheberrechtshinweis                                                         *
 *   Copyright © Luis Garcia 2018  (Kunstname: LuisDasNilpferd)                   *
 *   Erstellt: 12.01.2019 / 19:27                                                 *   
 *                                                                                *    
 *   Alle Inhalte dieses Quelltextes sind urheberrechtlich geschützt.             *
 *   Das Urheberrecht liegt, soweit nicht ausdrücklich anderes gekennzeichnet,    *
 *   bei Luis Garcia. Alle Rechte vorbehalten.                                    *
 *                                                                                *
 *   Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung,          *
 *   öffentlichen Zugänglichmachung oder andere Nutzung                           *
 *   bedarf der ausdrücklichen, schriftlichen Zustimmung von Luis Garcia.         *
 *********************************************************************************/

public class TeamChatCommand extends Command {

    private final BewerbungsSystem bewerbungsSystem;

    public TeamChatCommand(final BewerbungsSystem bewerbungsSystem) {
        super("teamchat");
        this.bewerbungsSystem = bewerbungsSystem;

        this.bewerbungsSystem.getPluginManager().registerCommand(this.bewerbungsSystem,this);
    }

    @Override
    public void execute(final CommandSender commandSender, final String[] args) {
        if(!(commandSender instanceof ProxiedPlayer))return;

        final ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;

        final StringBuilder stringBuilder = new StringBuilder();

        for(int i = 0; i < args.length; i++){
            stringBuilder.append(args[i]);
        }

        final String value = Data.PREFIX.toString() + proxiedPlayer.getDisplayName() + " §8➔ §7" + stringBuilder.toString().replaceAll("&","§");

        this.bewerbungsSystem.getProxy().getPlayers().forEach(players -> new Runnable() {
            public void run() {
                if(players.hasPermission("varox.teamchat")){
                    players.sendMessage(value);
                }
            }
        });

    }
}
